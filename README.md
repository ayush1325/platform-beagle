# platform-beagle

PlatformIO platform for BeagleConnect Freedom and other boards from BeagleBoard.org!

## Test

- Clone the platform to `~/.platformio/platforms/`
```shell
git clone https://openbeagle.org/beagleconnect/platform-beagle.git ~/.platformio/platforms/beagle
```

- Run example as shown in [README](./examples/zephyr-blink/README.md)
